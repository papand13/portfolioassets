# PortfolioAssets

PortfolioAssets is a project made as part of a coding exercise. It is a Windows desktop application made in WPF.

## Usage

Run the project using Visual Studio and try out the app. It can handle Currency and Stocks as assets.

The app retrieves current exchange rates from [api.exchangerate.host/latest](https://api.exchangerate.host/latest).