﻿using PortfolioAssets.Models;
using PortfolioAssets.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace PortfolioAssets
{
    /// <summary>
    /// Interaction logic for AddAssetDialog.xaml
    /// </summary>
    public partial class AddStockAssetDialog : Window
    {
        private readonly Dictionary<string, CurrencyModel> _currencies;

        public StockAssetModel StockAsset { get; set; }

        public AddStockAssetDialog()
        {
            InitializeComponent();
            _currencies = CurrencyUtil.GetCurrenciesAsync().Result;
            CurrencySelector.ItemsSource = _currencies.Select(x => x.Key);
            CurrencySelector.SelectedIndex = 0;
            SeparatorLabel.Text = CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator;
        }

        private void Cancel_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;

        private void Cancel_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void Add_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(SymbolInput.Text))
            {
                e.CanExecute = false;
                return;
            }

            if (string.IsNullOrWhiteSpace(ValueInput.Text) || !decimal.TryParse(ValueInput.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out var valuePerShare))
            {
                e.CanExecute = false;
                return;
            }

            if (string.IsNullOrWhiteSpace(SharesInput.Text) || !int.TryParse(SharesInput.Text, NumberStyles.Integer, CultureInfo.InvariantCulture, out var shareAmount))
            {
                e.CanExecute = false;
                return;
            }

            e.CanExecute = true;
        }

        private void Add_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            StockAsset = new()
            {
                Currency = _currencies[CurrencySelector.Text],
                Shares = Convert.ToInt32(SharesInput.Text, CultureInfo.InvariantCulture),
                Symbol = SymbolInput.Text.ToUpperInvariant(),
                Value = Convert.ToDecimal(ValueInput.Text, CultureInfo.InvariantCulture)
            };

            DialogResult = true;
            Close();
        }
    }

    public static class AddStockAssetCommands
    {
        public static readonly RoutedUICommand Cancel = new(nameof(Cancel), nameof(Cancel), typeof(AddStockAssetCommands),
            new InputGestureCollection()
            {
                new KeyGesture(Key.Escape)
            });

        public static readonly RoutedUICommand Add = new(nameof(Add), nameof(Add), typeof(AddStockAssetCommands),
            new InputGestureCollection()
            {
                new KeyGesture(Key.Enter)
            });
    }
}
