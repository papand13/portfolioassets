﻿using Microsoft.Win32;
using PortfolioAssets.Models;
using PortfolioAssets.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Windows;
using System.Windows.Input;

namespace PortfolioAssets
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private PortfolioModel _portfolioModel;
        private List<CurrencyModel> _currencies;
        private Dictionary<string, decimal> _exhangeRatesEur;

        public PortfolioModel PortfolioModel => _portfolioModel;
        public List<CurrencyModel> Currencies { get => _currencies; set => _currencies = value; }
        public CurrencyModel SelectedConsolidationCurrency { get; set; }
        public List<ConsolidatedStockAssetModel> ConsolidatedStockAssets
        {
            get
            {
                var consolidatedList = new List<ConsolidatedStockAssetModel>();

                var allStocks = _portfolioModel.StockAssets.Select(x => new StockAssetModel
                {
                    Currency = x.Currency,
                    Shares = x.Shares,
                    Symbol = x.Symbol,
                    Value = x.Value
                }).ToList();

                foreach (var stock in allStocks)
                {
                    if (stock.Currency.Code == SelectedConsolidationCurrency.Code)
                    {
                        continue;
                    }

                    if (stock.Currency.Code != "EUR")
                    {
                        stock.Value /= _exhangeRatesEur[stock.Currency.Code];
                    }

                    if (SelectedConsolidationCurrency.Code != "EUR")
                    {
                        stock.Value *= _exhangeRatesEur[SelectedConsolidationCurrency.Code];
                    }
                }

                var consolidatedStocks = allStocks.GroupBy(x => x.Symbol).Select(g => new ConsolidatedStockAssetModel
                {
                    Shares = g.Sum(x => x.Shares),
                    Value = g.Sum(x => x.TotalValue) / g.Sum(x => x.Shares),
                    Symbol = g.Key
                }).ToList();

                return consolidatedStocks;
            }
        }

        public List<ConsolidatedCurrencyAssetModel> ConsolidatedCurrencyAssets
        {
            get
            {
                var currencies = _portfolioModel.CurrencyAssets
                    .GroupBy(x => x.Currency.Code)
                    .Select(g => new ConsolidatedCurrencyAssetModel { OriginalCurrency = g.Key, OriginalValue = g.Sum(x => x.Value), ExchangedCurrency = g.Key, ExchangedValue = g.Sum(x => x.Value) })
                    .ToList();

                foreach (var currency in currencies)
                {
                    if (currency.OriginalCurrency == SelectedConsolidationCurrency.Code)
                    {
                        continue;
                    }

                    if (currency.OriginalCurrency != "EUR")
                    {
                        currency.ExchangedValue = currency.OriginalValue / _exhangeRatesEur[currency.OriginalCurrency];
                        currency.ExchangedCurrency = "EUR";
                    }

                    if (SelectedConsolidationCurrency.Code != "EUR")
                    {
                        currency.ExchangedValue *= _exhangeRatesEur[SelectedConsolidationCurrency.Code];
                        currency.ExchangedCurrency = SelectedConsolidationCurrency.Code;
                    }
                }

                return currencies;
            }
        }

        public decimal TotalPortfolioValue
        {
            get
            {
                return ConsolidatedStockAssets.Sum(x => x.TotalValue) + ConsolidatedCurrencyAssets.Sum(x => x.ExchangedValue);
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            _portfolioModel = new()
            {
                StockAssets = new ObservableCollection<StockAssetModel>(),
                CurrencyAssets = new ObservableCollection<CurrencyAssetModel>()
            };

            _currencies = CurrencyUtil.GetCurrenciesAsync().Result.Select(x => x.Value).ToList();
            _exhangeRatesEur = CurrencyUtil.GetLatestEurExchangeRateAsync().Result;

            DataContext = this;
            SelectedConsolidationCurrency = _currencies.Single(x => x.Code == "USD");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));

        private void RefreshConsolidationCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;

        private void RefreshConsolidationCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            NotifyPropertyChanged(nameof(ConsolidatedStockAssets));
            NotifyPropertyChanged(nameof(ConsolidatedCurrencyAssets));
            NotifyPropertyChanged(nameof(TotalPortfolioValue));
            NotifyPropertyChanged(nameof(SelectedConsolidationCurrency));
        }

        private void AddStockAssetCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;

        private void AddStockAssetCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new AddStockAssetDialog();
            var result = dlg.ShowDialog();
            if (result == false)
            {
                return;
            }

            _portfolioModel.StockAssets.Add(dlg.StockAsset);
            MainTabControl.SelectedIndex = 1;
            StockGrid.SelectedItem = dlg.StockAsset;
        }

        private void AddCurrencyAssetCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;

        private void AddCurrencyAssetCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new AddCurrencyAssetDialog();
            var result = dlg.ShowDialog();
            if (result == false)
                return;

            _portfolioModel.CurrencyAssets.Add(dlg.CurrencyAsset);
            MainTabControl.SelectedIndex = 2;
            CurrencyGrid.SelectedItem = dlg.CurrencyAsset;
        }

        private void RemoveAssetCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (MainTabControl == null)
            {
                e.CanExecute = false;
                return;
            }

            if (MainTabControl.SelectedIndex == 1)
            {
                e.CanExecute = StockGrid.SelectedItem != null;
                return;
            }

            if (MainTabControl.SelectedIndex == 2)
            {
                e.CanExecute = CurrencyGrid.SelectedItem != null;
                return;
            }
        }

        private void RemoveAssetCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (MainTabControl.SelectedIndex == 1)
            {
                _portfolioModel.StockAssets.RemoveAt(StockGrid.SelectedIndex);
                return;
            }

            if (MainTabControl.SelectedIndex == 2)
            {
                _portfolioModel.CurrencyAssets.RemoveAt(CurrencyGrid.SelectedIndex);
                return;
            }
        }

        private void SaveAssetsToFileCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;

        private async void SaveAssetsToFileCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new SaveFileDialog()
            {
                Title = "Save assets",
                AddExtension = true,
                DefaultExt = "pa",
                Filter = "Portfolio Assets|*.pa",
                OverwritePrompt = true
            };

            var result = dlg.ShowDialog(this);
            if (result == false)
            {
                return;
            }

            var fileName = dlg.FileName;
            var json = JsonSerializer.SerializeToUtf8Bytes(_portfolioModel);
            await File.WriteAllBytesAsync(fileName, json);
        }

        private void LoadAssetsFromFileCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;

        private async void LoadAssetsFromFileCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new OpenFileDialog()
            {
                Title = "Load assets",
                AddExtension = true,
                DefaultExt = "pa",
                Filter = "Portfolio Assets|*.pa"
            };

            var result = dlg.ShowDialog(this);
            if (result == false)
            {
                return;
            }

            var fileName = dlg.FileName;
            var json = await File.ReadAllTextAsync(fileName);
            _portfolioModel = JsonSerializer.Deserialize<PortfolioModel>(json);
            NotifyPropertyChanged(nameof(PortfolioModel));
        }
    }

    public static class MainWindowCommands
    {
        public static readonly RoutedUICommand Refresh = new(nameof(Refresh), nameof(Refresh), typeof(MainWindowCommands),
           new InputGestureCollection()
           {
                new KeyGesture(Key.F5)
           });


        public static readonly RoutedUICommand AddStock = new(nameof(AddStock), nameof(AddStock), typeof(MainWindowCommands),
            new InputGestureCollection()
            {
                new KeyGesture(Key.S, ModifierKeys.Control)
            });

        public static readonly RoutedUICommand AddCurrency = new(nameof(AddStock), nameof(AddStock), typeof(MainWindowCommands),
            new InputGestureCollection()
            {
                new KeyGesture(Key.C, ModifierKeys.Control)
            });

        public static readonly RoutedUICommand Remove = new(nameof(Remove), nameof(Remove), typeof(MainWindowCommands),
            new InputGestureCollection()
            {
                new KeyGesture(Key.N, ModifierKeys.Control)
            });
    }
}
