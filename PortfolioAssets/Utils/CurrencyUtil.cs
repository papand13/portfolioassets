﻿using PortfolioAssets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;

namespace PortfolioAssets.Utils
{
    public class CurrencyUtil
    {
        public static async Task<Dictionary<string, CurrencyModel>> GetCurrenciesAsync()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = assembly.GetManifestResourceNames().Single(str => str.EndsWith("currencies.json", StringComparison.InvariantCulture));
            using var stream = assembly.GetManifestResourceStream(resourceName);
            var currencies = await JsonSerializer.DeserializeAsync<Dictionary<string, CurrencyModel>>(stream);
            return currencies;
        }


        public static async Task<Dictionary<string, decimal>> GetLatestEurExchangeRateAsync()
        {
            var client = new HttpClient();
            var response = await client.GetAsync("https://api.exchangerate.host/latest").ConfigureAwait(false);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            var rates = JsonSerializer.Deserialize<ExchangeRatesResponseModel>(responseBody);
            return rates.Rates;
        }
    }
}
