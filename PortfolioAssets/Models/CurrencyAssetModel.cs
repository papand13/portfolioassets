﻿namespace PortfolioAssets.Models
{
    public class CurrencyAssetModel : IAsset
    {
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public CurrencyModel Currency { get; set; }
    }
}
