﻿using System.Text.Json.Serialization;

namespace PortfolioAssets.Models
{
    public class CurrencyModel
    {
        [JsonPropertyName("symbol")]
        public string Symbol { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("symbol_native")]
        public string SymbolNative { get; set; }

        [JsonPropertyName("decimal_digits")]
        public int DecimalDigits { get; set; }

        [JsonPropertyName("rounding")]
        public decimal Rounding { get; set; }

        [JsonPropertyName("code")]
        public string Code { get; set; }

        [JsonPropertyName("name_plural")]
        public string NamePlural { get; set; }
    }
}
