﻿namespace PortfolioAssets.Models
{
    public interface IAsset
    {
        /// <summary>
        /// The assets value in its given currency.
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// The currency of the asset.
        /// </summary>
        public CurrencyModel Currency { get; set; }
    }
}
