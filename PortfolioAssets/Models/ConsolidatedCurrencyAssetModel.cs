﻿namespace PortfolioAssets.Models
{
    public class ConsolidatedCurrencyAssetModel
    {
        public string OriginalCurrency { get; set; }
        public string ExchangedCurrency { get; set; }
        public decimal OriginalValue { get; set; }
        public decimal ExchangedValue { get; set; }
    }
}
