﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace PortfolioAssets.Models
{
    public class ExchangeRatesResponseModel
    {
        [JsonPropertyName("success")]
        public bool Success { get; set; }
        [JsonPropertyName("base")]
        public string Base { get; set; }
        [JsonPropertyName("date")]
        public string Date { get; set; }
        [JsonPropertyName("rates")]
        public Dictionary<string, decimal> Rates { get; set; }
    }
}
