﻿namespace PortfolioAssets.Models
{
    public class ConsolidatedStockAssetModel
    {
        public string Symbol { get; set; }
        public int Shares { get; set; }
        public decimal Value { get; set; }
        public decimal TotalValue => Value * Shares;
    }
}
