﻿using System;

namespace PortfolioAssets.Models
{
    public class StockAssetModel : IAsset
    {
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// The symbol for the stock.
        /// </summary>
        public string Symbol { get; set; }

        /// <summary>
        /// The amount of shares of the stock at this value.
        /// </summary>
        public int Shares { get; set; }

        /// <summary>
        /// The total value of all the stocks at this value and currency.
        /// </summary>
        public decimal TotalValue => Value * Shares;
    }
}
