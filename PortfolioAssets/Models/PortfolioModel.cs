﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace PortfolioAssets.Models
{
    public class PortfolioModel
    {
        /// <summary>
        /// All the currency assets in the portfolio.
        /// </summary>
        public ObservableCollection<CurrencyAssetModel> CurrencyAssets { get; set; }

        /// <summary>
        /// All the stock assets in the portfolio.
        /// </summary>
        public ObservableCollection<StockAssetModel> StockAssets { get; set; }
    }
}
